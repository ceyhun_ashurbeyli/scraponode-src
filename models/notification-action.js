var Sequelize = require('sequelize');

module.exports = function(sequelize){
    
    var Model = sequelize.define('notification_action', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: Sequelize.STRING
        }
    },{
        freezeTableName: true, // Model tableName will be the same as the model name
        timestamps: false ,
        underscored: true
    });

    return Model;
}

