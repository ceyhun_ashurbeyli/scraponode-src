var Sequelize = require('sequelize');

module.exports = function(sequelize){

	var Profile = require("./profile")(sequelize);
    var NotificationAction = require("./notification-action")(sequelize);
    var Product = require("./product")(sequelize);
	
	var Model = sequelize.define('notification', {
	    id:   {
	        type: Sequelize.INTEGER,
	        primaryKey: true,
	        autoIncrement: true
	    },
	    notification_id:
	    {
	        type: Sequelize.INTEGER,
	    },
	    user_one:                 {       type: Sequelize.INTEGER      },
	    user_two:                 {       type: Sequelize.INTEGER      },
	    product_id:               {       type: Sequelize.INTEGER      },
	    original:                 {       type: Sequelize.STRING       },
	    status:                   {       type: Sequelize.INTEGER      },
	    delivered:                {       type: Sequelize.INTEGER      },
	    date:                     {       type: Sequelize.DATE         },
	}, {
	    freezeTableName: true, // Model tableName will be the same as the model name
	    timestamps: false,
	    underscored: true
	});
	Model.belongsTo(NotificationAction, {foreignKey: 'notification_id', targetKey: 'id'}); //
	Model.belongsTo(Profile, {foreignKey: 'user_one', targetKey: 'user_id'}); //
	Model.belongsTo(Product, {foreignKey: 'product_id', targetKey: 'id'}); //

	return Model;
}