var Sequelize = require('sequelize');

module.exports = function(sequelize){
	var Profile = require("./profile")(sequelize);

	var Model = sequelize.define('product', {
	    id:             {
	        type: Sequelize.INTEGER,
	        primaryKey: true,
	        autoIncrement: true
	    },
	    name:           {   type: Sequelize.STRING           },
	    user_id:        {       type: Sequelize.INTEGER      },
	    main_image:     {       type: Sequelize.STRING       },
	    units:          {       type: Sequelize.STRING       },
	}, {
	    freezeTableName: true, // Model tableName will be the same as the model name
	    timestamps: false,
	    underscored: true
	});
	Model.belongsTo(Profile, {foreignKey: 'user_id', targetKey: 'user_id'});

	return Model;
}

