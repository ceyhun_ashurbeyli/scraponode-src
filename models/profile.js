var Sequelize = require('sequelize');

module.exports = function(sequelize){
	var Model = sequelize.define('profile', {
	    user_id:      {       type: Sequelize.INTEGER      },
	    online:       {       type: Sequelize.INTEGER      },
	    last_time:    {       type: Sequelize.DATE         },
	    firstname:    {       type: Sequelize.STRING       },
	    lastname:     {       type: Sequelize.STRING       },
	    profile_pic:  {       type: Sequelize.STRING       },
	    token:        {       type: Sequelize.STRING       },
	}, {
	    getterMethods   : {
	        fullName: function()  { return this.firstname + ' ' + this.lastname }
	    },
	    freezeTableName: true, // Model tableName will be the same as the model name
	    timestamps: false,
	    underscored: true
	});

	return Model;
}

