var Sequelize = require('sequelize');

module.exports = function(sequelize){
	var Model = sequelize.define('user_sessions', {
	    user_id:{
	    	type: Sequelize.INTEGER
	    },
	    date:{
	    	type: Sequelize.DATE
	    },
	}, {
	    freezeTableName: true, // Model tableName will be the same as the model name
	    timestamps: false,
	    underscored: true
	});

	return Model;
}

