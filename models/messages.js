var Sequelize = require('sequelize');

module.exports = function(sequelize){

    var Profile = require("./profile")(sequelize);
    var Conversation = require("./conversation")(sequelize);
    
    var Model = sequelize.define('messages', {
        id:   {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        user_id:                  {       type: Sequelize.INTEGER      },
        conversation_id:          {       type: Sequelize.INTEGER      },
        text:                     {       type: Sequelize.STRING       },
        type:                     {       type: Sequelize.STRING       },
        date:                     {       type: Sequelize.DATE         },
        delivered:                {       type: Sequelize.INTEGER      },
    }, {
        freezeTableName: true, // Model tableName will be the same as the model name
        timestamps: false,
        underscored: true
    });
    
    Model.belongsTo(Conversation, {foreignKey: 'conversation_id', targetKey: 'id'}); //
    Model.belongsTo(Profile, {foreignKey: 'user_id', targetKey: 'user_id'});

    return Model;
}

