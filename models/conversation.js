var Sequelize = require('sequelize');

module.exports = function(sequelize){
	
	var Model = sequelize.define('conversation', {
	    id: {
	        type: Sequelize.INTEGER,
	        primaryKey: true,
	        autoIncrement: true
	    },
	    from: {
	    	type: Sequelize.INTEGER
	    },
	    to: {
	    	type: Sequelize.INTEGER
	    }
	},{
	    freezeTableName: true, // Model tableName will be the same as the model name
	    timestamps: false ,
	    underscored: true
	});

	return Model;
}

