const usersInstance = require('../services/user');

module.exports  = {
	sprintf: function( format ){
		var users = usersInstance.getAll();
	    for( var i=1; i < arguments.length; i++ ) {
	        format = format.replace( /%s/, arguments[i] );
	    }
	    return format;
	},
	prepareEmit: function(data,action){
		var users = usersInstance.getAll();
	    if (users['u_'+data.opponentId]) {
	        for(socketId in users['u_'+data.opponentId]){
	            users['u_'+data.opponentId][socketId].emit(action, data);
	        }
	    }
	},
	prepareEmitMultipleUsers: function(data,action){
		var users = usersInstance.getAll();
	    data.data.partners.forEach(function(element){
	        if (users['u_'+element.user_id]) {
	            for(socketId in users['u_'+element.user_id]){
	                users['u_'+element.user_id][socketId].emit(action,data.data);
	            }
	        }
	    });
	},
	prepareEmitTo: function(data,action){
		var users = usersInstance.getAll();
	    if (users['u_'+data]) {
	        for(socketId in users['u_'+data]){
	            users['u_'+data][socketId].emit(action);
	        }
	    }
	}
};