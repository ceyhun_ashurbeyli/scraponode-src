const env = process.env.NODE_ENV || 'development';
const config = require('../config')[env];

var Log = function(msg){
	if(config.log){
		console.log(msg);
	}
}
module.exports = Log;