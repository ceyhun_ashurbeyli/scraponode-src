const http = require('http');
const sio = require('socket.io')(http);
const usersInstance = require('../services/user');

const Core = require('../modules/core');
const Log = require("../modules/log");

var io = null;
const self = this;

exports.io = function () {
  return io;
};
exports.disconnectListener = function(app){
	Log('Disconnect Listener Initialized')
	setInterval(function () {
	    var existSockets = [];

	    if(io && io.sockets && Object.keys(io.sockets.sockets).length > 0){
		    Object.keys(io.sockets.sockets).forEach(function (s) {
		        existSockets.push(s)
		    });
		}
	    var users = usersInstance.getAll();
	    for (var id in users) {
	        var i = 0;
	        var userIsOnline = false;
	        for(socketId in users[id]){
	            i++;
	            if(i != usersInstance.countUserSockets(id)){
	                if(existSockets.indexOf(users[id][socketId].id) >= 0) {
	                    userIsOnline = true;
	                }else{
	                	usersInstance.removeUserSocket(id, socketId);
	                    i--;
	                    userIsOnline = false;
	                }
	            }else{
	                if (existSockets.indexOf(users[id][socketId].id) >= 0) {
	                    userIsOnline = true;
	                }else{
	                    if(userIsOnline != true){
	                        usersInstance.remove(id);
	                        io.sockets.emit('set-offline', id.split("_")[1] ); // broadcast edilir
	                        app.settings.models.Profile.update( { 
	                        	'online': 0, 
	                        	'last_time' : app.settings.sequelize.fn('NOW')
	                        },{ 
	                        	where: { 
	                        		'user_id': parseInt(id.split("_")[1]) 
	                        	} 
	                        });
	                    }else{
	                        usersInstance.removeUserSocket(id, socketId);
	                        i--;
	                        userIsOnline = false;
	                    }
	                }
	            }
	        }
	    }
	}, 30000);
}
exports.init = function(server, app){
	var models = app.settings.models;
	io = sio.listen(server,{log:false, origins:'*:*'});

	io.sockets.on('connection', function (socket) {

	    var userId = parseInt(socket.handshake.query.userId); // istifadeci id-i
	    var token = socket.handshake.query.token; // istifadeci id-i
	    var u_userId = 'u_'+userId;

	    if(!isNaN(userId)) {
	        models.Profile.findOne({
	            where:{
	            	user_id: userId,token: token 
	            }
	        }).then(function (item) {
	            if(item){
	                // eger online istifadeci siyahisinda cari istifadeci yoxdursa, siyahiya elave ele, sql zapros gonder
	                if (!(usersInstance.exist(u_userId))) {

	                    usersInstance.add(u_userId, socket);

	                    Log("Connected UserId: " + userId);
	                    io.sockets.emit('set-online', userId);

	                    models.Profile.update( { 'online': 1 },{ where: { 'user_id': userId } } );
	                    models.UserSessions.findOrCreate({
	                      where: {
	                        user_id: parseInt(userId), 
	                        date: app.settings.sequelize.fn('CURDATE')
	                      },
	                      defaults: { // set the default properties if it doesn't exist
	                        user_id: parseInt(userId), 
	                        date: app.settings.sequelize.fn('CURDATE')
	                      }
	                    })
	                    
	                } else {
	                    usersInstance.updateBySocketId(u_userId, socket.id, socket);
	                }
	            }
	        });
	    }

	    socket.on('disconnect', function() {
	    	Log('disconnected');
	        if(usersInstance.countUserSockets(u_userId) >= 2){
	            usersInstance.removeUserSocket(u_userId,socket.id);
	        }
	    });



	    socket.on('new-message', function (data) {

	        if(!isNaN(data.id)) {
	            models.Messages.findOne({
	                where   :       {id: parseInt(data.id),delivered: 0,user_id : userId },
	                include :   [
	                    { model: models.Conversation, required: true },
	                    { model: models.Profile, required: true }
	                ]
	            }).then(function (item) {
	                if(item){
	                    if(item.conversation.from == userId){
	                        data.opponentId = item.conversation.to;
	                    }else{
	                        data.opponentId = item.conversation.from;
	                    }
	                    data.msg = item.text;
	                    data.type = item.type;
	                    data.date = item.date;
	                    data.dialogId = item.conversation_id;
	                    var sender = {
	                        'id': userId,
	                        'img': item.profile.profile_pic,
	                        'name': item.profile.fullName
	                    }
	                    data.sender = sender;
	                    data.userId = userId;
	                    item.delivered = 1;
	                    item.save().then(function(){
	                        Core.prepareEmit(data,'new-message');
	                    });
	                }else{
	                    Log('not ok');
	                }
	            });
	        }
	    });
	    socket.on('messages-read', function (dialogId) {
	        if(!isNaN(dialogId)) {
	            models.Conversation.findOne({
	                where   :       {id: parseInt(dialogId) },
	            }).then(function (item) {
	                if(item){
	                    var data = {}
	                    if(item.from == userId){
	                        data.opponentId = item.to;
	                    }else{
	                        data.opponentId = item.from;
	                    }
	                    data.dialogId = item.id;
	                    Core.prepareEmit(data,'messages-read');
	                }else{
	                    Log('not ok');
	                }
	            });
	        }
	    });
	    socket.on('conversation-on-typing', function (data) {
	        if(!isNaN(parseInt(data.conversation_id))) {
	            models.Conversation.findOne({
	                where   :   {
	                    id: parseInt(data.conversation_id),
	                    $or: [
	                        {   to: userId    },
	                        {   from: userId    },
	                    ]
	                }
	            }).then(function (item) {
	                if(item){
	                    if(item.to == userId){
	                        data.opponentId = item.from;
	                    }else{
	                        data.opponentId = item.to;
	                    }
	                    data.conversation_id = item.id;
	                    Core.prepareEmit(data, 'conversation-on-typing');
	                }else{
	                    Log('not ok');
	                }
	            });
	        }
	    });
	    socket.on('conversation-off-typing', function (data) {
	        if(!isNaN(parseInt(data.conversation_id))) {
	            models.Conversation.findOne({
	                where   :   {
	                    id: parseInt(data.conversation_id),
	                    $or: [
	                        {   to: userId    },
	                        {   from: userId    },
	                    ]
	                }
	            }).then(function (item) {
	                if(item){
	                    if(item.to == userId){
	                        data.opponentId = item.from;
	                    }else{
	                        data.opponentId = item.to;
	                    }
	                    data.conversation_id = item.id;
	                    Core.prepareEmit(data, 'conversation-off-typing');
	                }else{
	                    Log('not ok');
	                }
	            });
	        }
	    });
	    socket.on('new-notification', function (data) {
	        if(!isNaN(data.id)) {
	            models.Notification.findOne({
	                where   :       {id: parseInt(data.id),delivered: 0,status: 0, user_one : userId },
	                include :   [
	                    { model: models.NotificationAction, required: true },
	                    { model: models.Profile, required: true },
	                    { model: models.Product }
	                ]
	            }).then(function (notif) {
	                if(notif){
	                    if(notif.notification_id == 8 || notif.notification_id == 15){
	                        data.text = Core.sprintf(notif.notification_action.name, '<span class="link_text">'+notif.product.name+'</span>');
	                        data.link = '/product/'+notif.product.id;
	                    }else{
	                        if(notif.notification_id == 9 || notif.notification_id == 13 || notif.notification_id == 14 ||
	                            notif.notification_id == 18 || notif.notification_id == 19 || notif.notification_id == 20 || notif.notification_id == 21){
	                            data.link = '/profile/view/'+notif.user_one;
	                        }else if(notif.notification_id == 10 || notif.notification_id == 11){
	                            data.link = '/post/'+notif.original;
	                        }else if(notif.notification_id == 17){
	                            data.link = '/purchase-orders?bargainId='+notif.original;
	                        }
	                        data.text = notif.notification_action.name;
	                    }
	                    data.id = parseInt(data.id);
	                    data.opponentId = notif.user_two;
	                    data.userId = notif.user_one;
	                    data.image = notif.profile.profile_pic;
	                    data.name = notif.profile.fullName;
	                    data.date = notif.date;
	                    notif.delivered = 1;
	                    notif.save().then(function(){
	                        Core.prepareEmit(data,'new-notification');
	                    });
	                }else{
	                    Log('not ok');
	                }
	            });
	        }
	    });

	    socket.on('message', function(msg){
		    io.sockets.emit('message', msg)
	  	})
	});

	self.disconnectListener(app);
}