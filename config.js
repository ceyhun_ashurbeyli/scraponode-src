var config = {

	"development":{
		"url": "http://....",
		"log": false,
		"db":{
			"user": "root",
			"pass": "..",
			"host": "localhost",
			"name": "zupamart",
			"port": "3306",
		},
		"server":{
			"host": "localhost",
			"port":  "8080"
		},
	},
	"production":{
		"url": "https://node.scrapo.co",
		"log": false,
		"db":{
			"user": process.env.DB_USER,
			"pass": process.env.DB_PASS,
			"host": process.env.DB_HOST,
			"name": process.env.DB_NAME,
			"port": "3306",
		},
		"server":{
			"host": process.env.LISTEN_ADDR,
			"port":  process.env.LISTEN_PORT
		},
	},
	"staging":{
		"url": "https://staging-node.scrapo.co",
		"log": false,
		"db":{
			"user": process.env.DB_USER,
			"pass": process.env.DB_PASS,
			"host": process.env.DB_HOST,
			"name": process.env.DB_NAME,
			"port": "3306",
		},
		"server":{
			"host": process.env.LISTEN_ADDR,
			"port":  process.env.LISTEN_PORT
		},
	}
}
module.exports = config;