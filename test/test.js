var expect = require('chai').expect
	, config = require("../config")
	, io = require('socket.io/node_modules/socket.io-client')
	, ioOptionsSender = { 
		query: "userId=436&token=123", 
		secure: true, 
		forceNew: false
  	}
  	, ioOptionsReceiver = { 
		query: "userId=1075&token=qgOgKTtvivbZTIB5GVri2PyQcc_bevuk", 
		secure: true, 
		forceNew: false
  	}
  	, testMsg = 'HelloWorld'
  	, sender
  	, receiver;


describe('Chat Events', function(){

	beforeEach(function(done){
    
	    // start the io server
	    var app = require("../app");
	    // connect two io clients
	    sender = io(config.development.url + ':' + config.development.server.port + '/', ioOptionsSender)
	    receiver = io(config.development.url + ':' + config.development.server.port + '/', ioOptionsReceiver)
	    
	    // finish beforeEach setup
	    done()
  	})

	afterEach(function(done){
    
	    // disconnect io clients after each test
	    // sender.disconnect()
	    // receiver.disconnect()
	    done()
  	})

  	describe('Message Events', function(){
	    it('Clients should receive a message when the `message` event is emited.', function(done){
	      sender.emit('message', testMsg)
	      receiver.on('message', function(msg){
	        expect(msg).to.equal(testMsg)
	        done()
	      })
	    })
  	})

});