const fs = require('fs');
const http = require('http');
const express = require ('express');
const bodyParser = require('body-parser');
const PHPUnserialize = require('php-unserialize');
const Sequelize = require('sequelize');
const Log = require("./modules/log");
// Custom Modules
const Core = require("./modules/core");

// Loading Services
const usersInstance = require('./services/user');

// Initializing config
const env = process.env.NODE_ENV || 'development';
const config = require('./config')[env];

// Initializing application
const app = express();
const server = http.createServer(app);
app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
}));
// app.use(function(req, res, next) {
//     res.header("Access-Control-Allow-Origin", "*");
//     res.header("Access-Control-Allow-Headers", "X-Requested-With");
//     res.header("Access-Control-Allow-Headers", "Content-Type");
//     res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
//     next();
// });


// Sequelize Init
const sequelize = new Sequelize(
    config.db.name, 
    config.db.user, 
    config.db.pass,
    {
        port: config.db.port,
        host: config.db.host,
        logging: false,
        define: {
            timestamps: false
        }  
    }
);
sequelize
  .authenticate()
  .then(function(){
    Log('Connection has been established successfully.');
  })
  .catch(function(err){
    Log('Unable to connect to the database');
  });

// Initializing DB Models
app.set('sequelize',sequelize);
app.set('models',{
    User: require("./models/user")(sequelize),
    Profile: require("./models/profile")(sequelize),
    UserSessions: require("./models/user-sessions")(sequelize),
    Product: require("./models/product")(sequelize),
    Conversation: require("./models/conversation")(sequelize),
    Messages: require("./models/messages")(sequelize),
    NotificationAction: require("./models/notification-action")(sequelize),
    Notification: require("./models/notification")(sequelize)
})

// Initializing socket io 
var io = require("./modules/socketIO").init(server, app);


// Testing Get route
app.get('/', function(req, res,next) {  
    res.send('server is ok')
});
 

// Server Listener
server.listen(config.server.port);
server.on('listening', function() {
    Log('Express server started on port %s at %s', server.address().port, server.address().address);
});