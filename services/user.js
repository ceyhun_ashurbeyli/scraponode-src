const Log = require("../modules/log");

var users = [];

module.exports = {

	add: function(u_userId, socket){
		users[u_userId] = [];
		users[u_userId][socket.id] = socket;
	},

	remove: function(u_userId){
		delete users[u_userId];
		Log('- ' + u_userId.split("_")[1] + ' disconnected');
	},

	exist: function(u_userId){
		return users[u_userId] ? true : false
	},

	updateBySocketId: function(u_userId, socketId, socket){
		users[u_userId][socketId] = socket
	},

	countUserSockets: function(u_userId){
	    return Object.keys(users[u_userId]).length;
	},

	removeUserSocket: function(u_userId, socketId){
		delete users[u_userId][socketId];
	},

	getAll: function(){
		return users;
	},

};